﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerTrigger : MonoBehaviour
{
    public UnityEvent OnEnterTrigger;
    
    private void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "Player")
        {
            OnEnterTrigger.Invoke();
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        BoxCollider collider = GetComponent<BoxCollider>();

        Gizmos.color = new Color(1, 0.5f, 0, 0.4f);
        Gizmos.DrawCube(transform.position, Vector3.Scale(collider.size, transform.localScale));
    }
#endif
}
