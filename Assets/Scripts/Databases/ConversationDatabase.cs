﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public enum Characters
{
    NoOne,

    GreenGuy,
    Player,
    Zombie
}

public class ConversationDatabase : ScriptableObject
{
#if UNITY_EDITOR
    [MenuItem("Assets/Create/ConversationDatabase")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<ConversationDatabase>();
    }
#endif

    public List<ConversationDefinition> Definitions;
}

[Serializable]
public class ConversationDefinition
{
    public string ID;

    public Characters Character;
    public string Text;
    public bool ShowOnLeftSide;
}