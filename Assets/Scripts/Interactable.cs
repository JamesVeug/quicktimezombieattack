﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public ButtonType InterationButton;
    public QTButtonUI ButtonUI;

    private bool active;

    private void Update()
    {
        if (!active)
            return;

        if ((InterationButton == ButtonType.X && Input.GetButtonDown("x")) ||
            (InterationButton == ButtonType.Y && Input.GetButtonDown("y")) ||
            (InterationButton == ButtonType.B && Input.GetButtonDown("b")) ||
            (InterationButton == ButtonType.A && Input.GetButtonDown("a")))
        {
            Debug.Log("Interacted");
        }
    }

    public void Refresh()
    {
        ButtonType b = active ? InterationButton : ButtonType.None;
        ButtonUI.SetVisible(b, 0);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            active = true;
            Refresh();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            active = false;
            Refresh();
        }
    }
}
