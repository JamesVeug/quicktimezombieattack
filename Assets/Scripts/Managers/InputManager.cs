﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface InputListener
{
    void Axis(float x, float z);
    void ButtonPressed(ButtonType buttonType);
}


public class InputManager : MonoBehaviour
{
    public static InputManager Instance;

    private List<InputListener> Listeners = new List<InputListener>();

    private void Awake()
    {
        Instance = this;
    }

    public void FocusListener(InputListener listener)
    {
        Listeners.Add(listener);
    }

    public void ForgetListener(InputListener listener)
    {
        Listeners.Remove(listener);
    }

    private void Update()
    {
        if (Listeners.Count == 0)
        {
            return;
        }

        InputListener listener = Listeners[Listeners.Count - 1];

        // Axis
        var x = Input.GetAxis("Horizontal") * Time.deltaTime;
        var z = Input.GetAxis("Vertical") * Time.deltaTime;
        listener.Axis(x, z);

        // Button
        if (Input.GetButtonDown("x"))
        {
            listener.ButtonPressed(ButtonType.X);
        }
        else if (Input.GetButtonDown("y"))
        {
            listener.ButtonPressed(ButtonType.Y);
        }
        else if(Input.GetButtonDown("b"))
        {
            listener.ButtonPressed(ButtonType.B);
        }
        else if(Input.GetButtonDown("a"))
        {
            listener.ButtonPressed(ButtonType.A);
        }
    }
}
