﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelineManager : MonoBehaviour
{
    public static TimelineManager Instance;

    public PlayableDirector PlayOnAwake;

    private PlayableDirector playableDirector;

    private void Awake()
    {
        Instance = this;

        if (PlayOnAwake != null)
        {
            PlayTimeLine(PlayOnAwake);
        }
    }

    public void PlayTimeLine(PlayableDirector timeline)
    {
        playableDirector = timeline;
        timeline.Play();
    }

    public void Pause()
    {
        playableDirector.Pause();
    }

    public void Continue()
    {
        playableDirector.Play();
    }
}
