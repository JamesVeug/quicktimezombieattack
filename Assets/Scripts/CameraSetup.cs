﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSetup : MonoBehaviour
{
    public static CameraSetup CurrentSetup;

    public enum CameraRotatetionTypes
    {
        None,
        TrackCameraRotation,
        FollowPlayer
    }

    public bool StartActive;
    public Camera MainCamera;
    public CameraController CameraController;
    public CameraSetupOrigin[] trackPositions;
    public CameraRotatetionTypes CameraRotationType;

        

    private int index = 0;
    private bool active;

    private void Start()
    {
        CameraController.transform.position = trackPositions[index].cameraPosition.transform.position;

        if (StartActive)
        {
            Activate();
        }
    }

    public void Activate()
    {
        if (CurrentSetup == this)
        {
            return;
        }

        if (CurrentSetup != null)
        {
            CurrentSetup.Deactivate();
        }

        MainCamera.enabled = true;
        CurrentSetup = this;
        active = true;

        // Move to position
        float scalar = GetScaleAlongLine();

        if (index < trackPositions.Length - 1)
        {
            Vector3 position = GetNextPosition(scalar);
            // Set camera to move to position
            CameraController.transform.position = position;
        }
    }

    private void Deactivate()
    {
        MainCamera.enabled = false;
        active = false;
    }

    // Update is called once per frame
    void Update()
    {
        // Get position of player relative to tracks
        float scalar = GetScaleAlongLine();

        if (index < trackPositions.Length - 1)
        {
            Vector3 position = GetNextPosition(scalar);
            // Set camera to move to position
            CameraController.Position = position;
        }

        if (this.CameraRotationType == CameraRotatetionTypes.FollowPlayer)
        {
            Vector3 dir = PlayerController.Instance.transform.position - CameraController.transform.position;
            Quaternion to = Quaternion.LookRotation(dir);
            CameraController.Rotation = to;
        }
        else if (this.CameraRotationType == CameraRotatetionTypes.TrackCameraRotation)
        {
            if (index >= trackPositions.Length - 1)
            {
                CameraController.Rotation = trackPositions[index].cameraPosition.transform.rotation;
            }
            else
            {
                Quaternion from = trackPositions[index].cameraPosition.transform.rotation;
                Quaternion to = trackPositions[index + 1].cameraPosition.transform.rotation;
                CameraController.Rotation = Quaternion.Lerp(from, to, scalar);
            }
        }
        else
        {
            CameraController.Rotation = CameraController.transform.rotation;
        }
    }

    private float GetScaleAlongLine()
    {
        if (index >= trackPositions.Length -1)
        {
            return 0;
        }

        Vector3 currentTrackPosition = trackPositions[index].trackPosition.transform.position;
        Vector3 nextTrackPosition = trackPositions[index + 1].trackPosition.transform.position;
        Vector3 playerPosition = PlayerController.Instance.transform.position;
        Vector3 trackedPosition = NearestPointOnFiniteLine(currentTrackPosition, nextTrackPosition, playerPosition);
        float distance = Vector3.Distance(currentTrackPosition, trackedPosition);
        float pathDistance = Vector3.Distance(currentTrackPosition, nextTrackPosition);
        float scalar = Mathf.Clamp01(distance / pathDistance);
        return scalar;
    }

    private Vector3 GetNextPosition(float scalar)
    {
        //Debug.Log("Distace: " + distance);
        //Debug.Log("Scalar: " + scalar);

        // Get position of camera relative to player tracked position
        Vector3 camCurrentTrackPosition = trackPositions[index].cameraPosition.transform.position;
        if (index >= trackPositions.Length - 1)
        {
            return camCurrentTrackPosition;
        }

        Vector3 camNextTrackPosition = trackPositions[index + 1].cameraPosition.transform.position;
        Vector3 camPath = camNextTrackPosition - camCurrentTrackPosition;
        Vector3 position = camCurrentTrackPosition + camPath * scalar;
        return position;
    }

    //linePnt - point the line passes through
    //lineDir - unit vector in direction of line, either direction works
    //pnt - the point to find nearest on line for
    public static Vector3 NearestPointOnLine(Vector3 linePnt, Vector3 lineDir, Vector3 pnt)
    {
        lineDir.Normalize();//this needs to be a unit vector
        var v = pnt - linePnt;
        var d = Vector3.Dot(v, lineDir);
        return linePnt + lineDir * d;
    }

    public static Vector3 NearestPointOnFiniteLine(Vector3 start, Vector3 end, Vector3 pnt)
    {
        var line = (end - start);
        var len = line.magnitude;
        line.Normalize();

        var v = pnt - start;
        var d = Vector3.Dot(v, line);
        d = Mathf.Clamp(d, 0f, len);
        return start + line * d;
    }
}
