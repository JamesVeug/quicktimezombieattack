﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCController : MonoBehaviour
{
    public Vector3 Speed;
    public CharacterController CharacterController;
    public QTButtonUI QTButtonUI;

    // Update is called once per frame
    void Update()
    {
        CharacterController.SimpleMove(Vector3.zero);
    }


    public void RefreshQUButtonUI(ButtonType buttonType, float maxTime, float time)
    {
        QTButtonUI.SetVisible(buttonType, maxTime, time);
    }
}
