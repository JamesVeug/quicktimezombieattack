﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public CharacterController controller;

    public float MoveSpeed;
    public float RotateSpeed;
    public Vector3 Position;
    public Quaternion Rotation;

    // Start is called before the first frame update
    void Awake()
    {
        Position = transform.position;
    }

    // Update is called once per framet
    void LateUpdate()
    {
        Vector3 path = Position - transform.position;
        float distance = path.magnitude;
        if (distance < 0.1)
        {
            transform.position = Position;
        }
        else
        {
            controller.Move(path.normalized * Time.deltaTime * MoveSpeed);
        }

        if (Quaternion.Dot(transform.rotation, Rotation) > 0.1)
        {
            transform.rotation = Rotation;
        }
    }
}
