﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class TalkingMenuClip : PlayableAsset, ITimelineClipAsset
{
    [SerializeField]
    private TalkingMenuBehaviour Conversation = new TalkingMenuBehaviour();

    public ClipCaps clipCaps
    {
        get {
            return ClipCaps.None;
        }
    }

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        return ScriptPlayable<TalkingMenuBehaviour>.Create(graph, Conversation);
    }
}
