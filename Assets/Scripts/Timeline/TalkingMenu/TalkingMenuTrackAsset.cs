﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

[TrackBindingType(typeof(TalkingMenuUI))]
[TrackClipType(typeof(TalkingMenuClip))]
public class TalkingMenuTrackAsset : TrackAsset
{
    
}
