﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[Serializable]
public class TalkingMenuBehaviour : PlayableBehaviour
{
    public Characters LeftCharacter { get { return m_leftCharacter; } }
    public Characters RightCharacter { get { return m_rightCharacter; } }
    public bool TalkingCharacterLeft { get { return m_talkingCharacterLeft; } }
    public string Text { get { return m_text; } }

    [SerializeField]
    private Characters m_leftCharacter;

    [SerializeField]
    private Characters m_rightCharacter;

    [SerializeField]
    private bool m_talkingCharacterLeft;

    [SerializeField]
    private string m_text;

    private bool m_firstFrameHappened;
    private bool m_paused;
    private bool m_unpaused = true;
    private Characters m_defaultLeftCharacter;
    private Characters m_defaultRightCharacter;
    private Characters m_defaultTalkingCharacter;
    private string m_defaultText;

    private TalkingMenuUI ui;

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        ui = playerData as TalkingMenuUI;
        if (ui == null)
        {
            return;
        }

        if (!m_firstFrameHappened)
        {
            m_firstFrameHappened = false;
            m_unpaused = false;
        }

        
        double duration = playable.GetDuration();
        double t = playable.GetTime();
        double delta = t / duration;
        if (ui.Populate(this, delta))
        {
            if (!m_paused)
            {
                if (TimelineManager.Instance != null)
                {
                    TimelineManager.Instance.Pause();
                }
                m_paused = true;
            }
            else
            {
                m_unpaused = true;
            }
        }
    }

    public override void OnBehaviourPlay(Playable playable, FrameData info)
    {
        base.OnBehaviourPlay(playable, info);
    }

    public override void OnBehaviourPause(Playable playable, FrameData info)
    {
#if UNITY_EDITOR
        bool isPlaying = !Application.isPlaying || m_unpaused;
#else
        bool isPlaying = m_unpaused;
#endif

        if (ui != null && isPlaying)
        {
            m_firstFrameHappened = false;

            ui.Populate(null, 0);
        }

        base.OnBehaviourPause(playable, info);
    }
}
