﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShootingUI : MonoBehaviour, InputListener
{
    private class TargetData
    {
        public NPCController npc;
        public float TotalTime;
        public float DeltaTime;
    }

    private bool active;
    private TargetData currentTarget = null;
    private List<TargetData> targets = new List<TargetData>();

    public void Initialize()
    {

    }

    private void Update()
    {
        if (!active)
            return;

        // Choose target
        if (currentTarget == null && targets.Count > 0)
        {
            currentTarget = targets[0];
        }

        for (int i = 0; i < targets.Count; i++)
        {
            TargetData data = targets[i];
            if (data.DeltaTime < data.TotalTime)
            {
                data.DeltaTime += Time.deltaTime;
                if (data.DeltaTime >= data.TotalTime)
                {
                    SetToggled(false);

                }
            }
        }


    }

    private void Populate()
    {

    }

    public void AddTarget(NPCController npc, float time)
    {
        TargetData data = new TargetData();
        data.npc = npc;
        data.TotalTime = time;
        targets.Add(data);
    }

    public void SetToggled(bool active)
    {
        this.active = active;
    }
    
    public void Axis(float x, float z)
    {
        // Nothing
    }

    public void ButtonPressed(ButtonType buttonType)
    {
    }
}
