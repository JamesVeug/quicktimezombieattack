﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TalkingMenuUI : MonoBehaviour, InputListener
{
    public Animator Animator;
    public Sprite PlayerSprite;
    public Sprite GreenGuySprite;
    public Sprite ZombieSprite;

    public ButtonType ContinueButtonType;
    public GameObject Container;
    public Image LeftImage;
    public Image RightImage;
    public Text NameText;
    public Text SpeechText;

    public QTButtonUI QTButton;
    public Text QTButtonText;

    private TalkingMenuBehaviour cachedBehaviour;
    private List<ConversationDefinition> conversations;
    private int index;

    private void Awake()
    {
        Populate();
    }

    public void Initialize(List<ConversationDefinition> conversations)
    {
        this.conversations = conversations;
        this.index = 0;
        Populate();
    }

    public void ShowNext()
    {
        index++;
        Populate();
    }

    private void Populate()
    {
        if (conversations == null || index >= conversations.Count)
        {
            Container.SetActive(false);
            InputManager.Instance.ForgetListener(this);
        }
        else
        {
            QTButton.SetVisible(ContinueButtonType, 0);
            Container.SetActive(true);
            ConversationDefinition conversation = conversations[index];
            LeftImage.gameObject.SetActive(conversation.ShowOnLeftSide);
            RightImage.gameObject.SetActive(!conversation.ShowOnLeftSide);
            NameText.text = GetCharacterName(conversation.Character);
            SpeechText.text = conversation.Text;

            QTButtonText.text = "Continue";
        }
    }

    public bool Populate(TalkingMenuBehaviour behaviour, double deltaTime)
    {
        if (behaviour == null)
        {
            // Default
            if (InputManager.Instance != null) InputManager.Instance.ForgetListener(this);
            QTButton.SetVisible(ButtonType.X, 0);
            Container.SetActive(false);
            LeftImage.gameObject.SetActive(true);
            LeftImage.sprite = GetCharacterSprite(Characters.Player);
            RightImage.sprite = GetCharacterSprite(Characters.GreenGuy);
            RightImage.gameObject.SetActive(true);
            NameText.text = GetCharacterName(Characters.Player);
            SpeechText.text = "Text Text!";
            QTButtonText.text = "Continue";
            cachedBehaviour = behaviour;
            return false;
        }
        else
        {
            if(InputManager.Instance != null) InputManager.Instance.FocusListener(this);
            QTButton.SetVisible(ContinueButtonType, 0);
            Container.SetActive(true);
            if (behaviour.LeftCharacter != Characters.NoOne)
            {
                LeftImage.gameObject.SetActive(true);
                LeftImage.sprite = GetCharacterSprite(behaviour.LeftCharacter);
            }
            else
            {
                LeftImage.gameObject.SetActive(false);
            }

            if (behaviour.RightCharacter != Characters.NoOne)
            {
                RightImage.gameObject.SetActive(true);
                RightImage.sprite = GetCharacterSprite(behaviour.RightCharacter);
            }
            else
            {
                RightImage.gameObject.SetActive(false);
            }

            if (behaviour != cachedBehaviour)
            {
                if (behaviour.TalkingCharacterLeft)
                {
                    Animator.SetTrigger("BounceLeft");
                }
                else
                {
                    Animator.SetTrigger("BounceRight");
                }
            }
        
            NameText.text = GetCharacterName(behaviour.TalkingCharacterLeft ? behaviour.LeftCharacter : behaviour.RightCharacter);

            int textAmount = Mathf.CeilToInt((float)(behaviour.Text.Length * deltaTime));
            string subbed = behaviour.Text.Substring(0, textAmount);
            SpeechText.text = subbed;
            QTButtonText.text = "Continue";
            cachedBehaviour = behaviour;
            return subbed == behaviour.Text;
        }
    }

    private Sprite GetCharacterSprite(Characters character)
    {
        switch (character)
        {
            case Characters.GreenGuy:
                return GreenGuySprite;
            case Characters.Player:
                return PlayerSprite;
            case Characters.Zombie:
                return ZombieSprite;
            default:
                return null;
        }
    }

    private string GetCharacterName(Characters character)
    {
        switch (character)
        {
            case Characters.GreenGuy:
                return "Green Guy";
            case Characters.Player:
                return "You";
            case Characters.Zombie:
                return "ZomZom";
            default:
                return "Unknown Enum " + character;
        }
    }

    public void Axis(float x, float z)
    {
        // Nothing
    }

    public void ButtonPressed(ButtonType buttonType)
    {
        if (ContinueButtonType == buttonType)
        {
            TimelineManager.Instance.Continue();
        }
    }
}
