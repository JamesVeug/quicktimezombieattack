﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QTButtonUI : MonoBehaviour
{
    public GameObject Container = null;
    public Image XImage = null;
    public Image YImage = null;
    public Image AImage = null;
    public Image BImage = null;
    public bool RotateToFacePlayer = true;

    private Image image = null;
    private bool visible = false;

    private float maxTime;
    private float deltaTime;

    private void Awake()
    {
        SetVisible(ButtonType.None, 0, 0);
    }

    public void SetVisible(ButtonType buttonType, float maxTime, float deltaTime)
    {
        bool visible = buttonType != ButtonType.None;
        Container.SetActive(visible);
        this.visible = visible;

        if (image != null)
        {
            image.gameObject.SetActive(false);
        }

        switch (buttonType)
        {
            case ButtonType.X:
                image = XImage;
                break;
            case ButtonType.Y:
                image = YImage;
                break;
            case ButtonType.A:
                image = AImage;
                break;
            case ButtonType.B:
                image = BImage;
                break;
            default:
                image = null;
                break;
        }

        if (image != null)
        {
            image.gameObject.SetActive(true);
        }

        this.maxTime = maxTime;
        this.deltaTime = 0;

        if (image != null)
        {
            if (maxTime > 0)
            {
                float scale = deltaTime / maxTime;
                this.image.fillAmount = scale;
            }
            else
            {
                this.image.fillAmount = 0;
            }
        }
    }

    void LateUpdate()
    {
        if (visible && RotateToFacePlayer)
        {
            // look at the camera
            Vector3 dir = CameraSetup.CurrentSetup.MainCamera.transform.position - transform.position;
            dir.z = 0;
            transform.rotation = Quaternion.Euler(dir);
        }
    }
}
