﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour, InputListener
{
    public static PlayerController Instance;

    public Vector3 Speed;
    public CharacterController CharacterController;
    
    private void Start()
    {
        Instance = this;
        InputManager.Instance.FocusListener(this);
    }

    // Update is called once per frame
    public void Axis(float x, float z)
    {
        Vector3 pos = transform.forward * z * Speed.z;

        CharacterController.SimpleMove(pos);
        transform.Rotate(0, x * Speed.x, 0);
        //transform.Translate(0, 0, z);
    }

    public void ButtonPressed(ButtonType buttonType)
    {
        // Nothing atm
    }
}
